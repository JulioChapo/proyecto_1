﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Logia
{
    public class Venta
    {
        public int CodigoOrteosisVendida { get; set; }
        public int DNICliente { get; set; }
        public DateTime FechaVenta { get; set; }
        public double PorcentajeRecargoAplicado { get; set; }
        public double PrecioTotalCalculado { get; set; }
    }
}
