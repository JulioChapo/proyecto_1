﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Logia
{
    public class Muñeca: Ortesis
    {
        public bool Universal { get; set; }
        public bool SoportePulgar { get; set; }
        public bool MovimientoActivo { get; set; }

        public override double CalcularPorcentajeRecargo()
        {
            if ( SoportePulgar == true)
            {
                return Acumulador += PrecioUnitario * 0.03;
            }
            return Acumulador;
        }
    }
}
